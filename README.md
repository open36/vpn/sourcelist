# sourcelist
- gfw
https://gitlab.com/open36/vpn/sourcelist/-/raw/main/black.list

- 中国IP段(chnroute)更新URL
https://gitlab.com/open36/vpn/sourcelist/-/raw/main/white.ip


- 中国域名列表(Chnlist)更新URL
https://gitlab.com/open36/vpn/sourcelist/-/raw/main/white.list










# https://gitlab.com/open36/vpn/sourcelist/-/raw/main/balck.list.txt

private token: TxHwEYqkVVqagdLyb4Qx

## gfwlist

url

```
https://gitlab.com/hong3e_working/vpn/sourcelist/-/raw/main/black.list?private_token=TxHwEYqkVVqagdLyb4Qx
```

download

```
wget-ssl --no-check-certificate -t 3 -T 10 -O- https://gitlab.com/hong3e_working/vpn/sourcelist/-/raw/main/black.list?private_token=TxHwEYqkVVqagdLyb4Qx > black.list
```

### change source

```
vi /usr/share/shadowsocksr/gfw2ipset.sh
```

```
wget-ssl --no-check-certificate -t 3 -T 10 -O- https://gitlab.com/hong3e_working/vpn/sourcelist/-/raw/main/black.list?private_token=TxHwEYqkVVqagdLyb4Qx > /tmp/tmp/black.list
wget-ssl --no-check-certificate -t 3 -T 10 -O- https://gitlab.com/hong3e_working/vpn/sourcelist/-/raw/main/white.list?private_token=TxHwEYqkVVqagdLyb4Qx > /tmp/tmp/white.list
wget-ssl --no-check-certificate -t 3 -T 10 -O- https://gitlab.com/hong3e_working/vpn/sourcelist/-/raw/main/deny.list?private_token=TxHwEYqkVVqagdLyb4Qx > /tmp/tmp/deny.list


for line in $(cat /etc/ssr/black.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_list.conf; done
for line in $(cat /etc/ssr/black.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_base.conf; done
for line in $(cat /tmp/tmp/black.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_list.conf; done
for line in $(cat /tmp/tmp/black.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_base.conf; done

for line in $(cat /etc/ssr/white.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_list.conf; done
for line in $(cat /etc/ssr/white.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_base.conf; done
for line in $(cat /tmp/tmp/white.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_list.conf; done
for line in $(cat /tmp/tmp/white.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_base.conf; done

for line in $(cat /etc/ssr/deny.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_list.conf; done
for line in $(cat /etc/ssr/deny.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_base.conf; done
for line in $(cat /tmp/tmp/deny.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_list.conf; done
for line in $(cat /tmp/tmp/deny.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/gfw_base.conf; done



awk '!/^$/&&!/^#/{printf("ipset=/%s/'"blacklist"'\n",$0)}' /etc/ssr/black.list >/tmp/dnsmasq.ssr/blacklist_forward.conf
awk '!/^$/&&!/^#/{printf("server=/%s/'"127.0.0.1#5335"'\n",$0)}' /etc/ssr/black.list >>/tmp/dnsmasq.ssr/blacklist_forward.conf
awk '!/^$/&&!/^#/{printf("ipset=/%s/'"blacklist"'\n",$0)}' /tmp/tmp/black.list >>/tmp/dnsmasq.ssr/blacklist_forward.conf
awk '!/^$/&&!/^#/{printf("server=/%s/'"127.0.0.1#5335"'\n",$0)}' /tmp/tmp/black.list >>/tmp/dnsmasq.ssr/blacklist_forward.conf


awk '!/^$/&&!/^#/{printf("ipset=/%s/'"whitelist"'\n",$0)}' /etc/ssr/white.list >/tmp/dnsmasq.ssr/whitelist_forward.conf
awk '!/^$/&&!/^#/{printf("ipset=/%s/'"whitelist"'\n",$0)}' /tmp/tmp/white.list >>/tmp/dnsmasq.ssr/whitelist_forward.conf

awk '!/^$/&&!/^#/{printf("address=/%s/''\n",$0)}' /etc/ssr/deny.list >/tmp/dnsmasq.ssr/denylist.conf
awk '!/^$/&&!/^#/{printf("address=/%s/''\n",$0)}' /tmp/tmp/deny.list >>/tmp/dnsmasq.ssr/denylist.conf


if [ "$(uci_get_by_type global adblock 0)" == "1" ]; then
	[ "$1" == "" ] && cp -f /etc/ssr/ad.conf /tmp/dnsmasq.ssr/
	if [ -f "/tmp/dnsmasq.ssr/ad.conf" ]; then
		for line in $(cat /etc/ssr/black.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/ad.conf; done
		for line in $(cat /tmp/tmp/black.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/ad.conf; done
		for line in $(cat /etc/ssr/white.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/ad.conf; done
		for line in $(cat /tmp/tmp/white.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/ad.conf; done
		for line in $(cat /etc/ssr/deny.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/ad.conf; done
		for line in $(cat /tmp/tmp/deny.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/ad.conf; done

		for line in $(cat /etc/ssr/netflix.list); do sed -i "/$line/d" /tmp/dnsmasq.ssr/ad.conf; done
	fi
fi
```
